package com.my.test;

import com.my.serialException.*;
import com.my.utils.SerialTool;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * @program: later10_9
 * @Date: 2018.10.13 上午 12:54
 * @Author: MicoMecy
 */
public class Test {
    public static void main(String[] args) throws SerialPortParameterFailure, NoSuchPort, PortInUse, NotASerialPort, ReadDataFromSerialPortFailure, SerialPortInputStreamCloseFailure {
        SerialTool serialTool = SerialTool.getSerialTool();
        ArrayList<String> portList = SerialTool.findPort();
        int baudrate = 9600;
        String port = portList.get(0);
//        System.out.println(s);
//        打开串口,获得串口对象
        SerialPort serialPort = SerialTool.openPort(port, baudrate);
//        从串口读数据
        String uart_Str = "";

        while (true) {
            byte[] bytes = SerialTool.readFromPort(serialPort);
            if (bytes != null) {
                uart_Str = new String(bytes);
                System.out.println(uart_Str);
            }

        }

        //关闭串口
//        SerialTool.closePort(serialPort);

    }
}
